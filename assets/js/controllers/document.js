angular.module('mdown').controller('DocumentCtrl', function($scope, $rootScope, $state, $http, $location, Document, Editor) {
  var $content, key, manager, save;
  console.log("DocumentCtrl");
  key = $state.params.key;
  manager = new Manager;
  save = _.debounce(function(doc) {
    var cmd;
    cmd = new UpdateDocumentCommand;
    cmd.params.set('document', doc);
    cmd.params.set('editor', Editor.editor);
    return manager["do"](cmd);
  }, 1000);
  Editor.load();
  Mousetrap.bind(['command+z', 'ctrl+z'], function() {
    manager.undo();
    return false;
  });
  Mousetrap.bind(['command+y', 'ctrl+y'], function() {
    manager.redo();
    return false;
  });
  Document.close();
  Document.read(key, function(err, doc) {
    if (err) {
      return $state.transitionTo('document:error');
    } else {
      Editor.editor.setValue(doc.get('content'));
      Mousetrap.bind(['command+s', 'ctrl+s'], function() {
        save(doc);
        return false;
      });
      return Editor.editor.addKeyMap({
        "Cmd-S": function() {
          save(doc);
          return false;
        },
        "Ctrl-S": function() {
          save(doc);
          return false;
        },
        "Cmd-Z": function() {
          manager.undo();
          return false;
        },
        "Ctrl-Z": function() {
          manager.undo();
          return false;
        },
        "Cmd-Y": function() {
          manager.redo();
          return false;
        },
        "Ctrl-Y": function() {
          manager.redo();
          return false;
        }
      });
    }
  });
  $content = $('.content-wrapper');
  return $content.on('click', function() {
    return Editor.editor.focus();
  });
});
