angular.module('mdown').controller('NavigationCtrl', function($scope, $rootScope, $state, Document, Editor) {
  var $content;
  console.log("NavigationCtrl");
  $content = $('.content-wrapper');
  $scope.document = Document.toJSON();
  Document.on('read', function() {
    return $scope.document = Document.toJSON();
  });
  Document.on('close', function() {
    return $scope.document = Document.toJSON();
  });
  $scope.second = {};
  $scope.third = {};
  $scope.second.open = function() {
    $scope.second.state = true;
    return $content.addClass('content-navigation-second');
  };
  $scope.second.close = function() {
    $scope.second.state = false;
    $scope.third.state = false;
    return $content.removeClass('content-navigation-second content-navigation-third');
  };
  $scope.third.open = function() {
    $scope.second.state = true;
    $scope.third.state = true;
    return $content.addClass('content-navigation-second content-navigation-third');
  };
  $scope.third.close = function() {
    $scope.third.state = false;
    return $content.removeClass('content-navigation-second content-navigation-third');
  };
  $scope.edit = {};
  $scope.edit.toggle = function() {
    return $scope.second.open();
  };
  $scope.edit.document = {};
  $scope.edit.document.create = function() {
    return Document.create(function(err, doc) {
      return $state.transitionTo('document', {
        key: doc.get('key')
      });
    });
  };
  $scope.edit.document.update = function() {
    if (Document.editable()) {
      return Document.update(function() {
        return console.log("Document updated manually");
      });
    }
  };
  Mousetrap.bind(['alt+space'], function() {
    if ($scope.second.state) {
      return $scope.second.close();
    } else {
      return $scope.second.open();
    }
  });
  return Editor.on('load', function(editor) {
    console.log("editor load");
    return editor.addKeyMap({
      "Alt-Space": function() {
        console.log("alt");
        if ($scope.second.state) {
          $scope.second.close();
        } else {
          $scope.second.open();
        }
        return false;
      }
    });
  });
});
