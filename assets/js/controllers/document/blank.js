angular.module('mdown').controller('DocumentBlankCtrl', function($scope, Document, $state) {
  console.log("DocumentBlankCtrl");
  Document.close();
  return $scope.create = function() {
    return Document.create(function(err, doc) {
      return $state.transitionTo('document', {
        key: doc.get('key')
      });
    });
  };
});
