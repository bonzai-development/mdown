angular.module('mdown').controller('EditorCtrl', function($scope, $rootScope) {
  var editor;
  return editor = CodeMirror.fromTextArea($('#editor')[0], {
    mode: "markdown",
    viewportMargin: Infinity
  });
});
