var UpdateDocumentCommand, _ref,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

UpdateDocumentCommand = (function(_super) {
  __extends(UpdateDocumentCommand, _super);

  function UpdateDocumentCommand() {
    _ref = UpdateDocumentCommand.__super__.constructor.apply(this, arguments);
    return _ref;
  }

  UpdateDocumentCommand.prototype.init = function() {
    this.required.set('document');
    return this.required.set('editor');
  };

  UpdateDocumentCommand.prototype["do"] = function() {
    var document, editor;
    document = this.params.get('document');
    editor = this.params.get('editor');
    if (document.get('content') !== editor.getValue()) {
      this.storage.set('undo', document.get('content'));
      document.set('content', editor.getValue());
      document.update(function(err, doc) {
        return console.log("Document update do");
      });
      return true;
    }
    return false;
  };

  UpdateDocumentCommand.prototype.undo = function() {
    var document, editor;
    document = this.params.get('document');
    editor = this.params.get('editor');
    this.storage.set('redo', document.get('content'));
    document.set('content', this.storage.get('undo'));
    editor.setValue(document.get('content'));
    return document.update(function(err, doc) {
      return console.log("Document update undo");
    });
  };

  UpdateDocumentCommand.prototype.redo = function() {
    var document, editor;
    document = this.params.get('document');
    editor = this.params.get('editor');
    document.set('content', this.storage.get('redo'));
    editor.setValue(document.get('content'));
    return document.update(function(err, doc) {
      return console.log("Document update do");
    });
  };

  return UpdateDocumentCommand;

})(Command);
