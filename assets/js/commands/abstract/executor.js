var Executor;

Executor = (function() {
  function Executor(command) {
    this.command = command;
  }

  Executor.prototype.isExecutable = function(command) {
    var executable, key, value, _ref;
    executable = true;
    _ref = command.required.get();
    for (key in _ref) {
      value = _ref[key];
      executable = executable && key in command.params.get();
    }
    return executable;
  };

  Executor.prototype.getMissingParams = function(command) {
    var key, missing, value, _ref;
    missing = [];
    _ref = command.required.get();
    for (key in _ref) {
      value = _ref[key];
      if (!(key in command.params.get())) {
        missing.push(key);
      }
    }
    return missing;
  };

  Executor.prototype["do"] = function() {
    if (!this.isExecutable(this.command)) {
      throw new Error("Do method is not executable. Missing params: " + this.getMissingParams(this.command).join(', '));
    }
    return this.command["do"]();
  };

  Executor.prototype.undo = function() {
    if (!this.isExecutable(this.command)) {
      throw new Error("Undo method is not executable. Missing params: " + this.getMissingParams(this.command).join(', '));
    }
    return this.command.undo();
  };

  Executor.prototype.redo = function() {
    if (!this.isExecutable(this.command)) {
      throw new Error("Redo (or do) method is not executable. Missing params: " + this.getMissingParams(this.command).join(', '));
    }
    if (this.command.redo) {
      return this.command.redo();
    } else {
      return this.command["do"]();
    }
  };

  return Executor;

})();
