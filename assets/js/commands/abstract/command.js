var Command;

Command = (function() {
  function Command() {
    this.params = new Bundle();
    this.required = new Bundle();
    this.storage = new Bundle();
    this.execute = new Executor(this);
    if (typeof this.init === "function") {
      this.init();
    }
  }

  Command.prototype["do"] = function() {
    throw new Error("Unimplemented do method.");
  };

  Command.prototype.undo = function() {
    throw new Error("Unimplemented undo method.");
  };

  return Command;

})();
