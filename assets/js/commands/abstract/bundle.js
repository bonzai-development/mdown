var Bundle;

Bundle = (function() {
  function Bundle() {
    this.values = {};
  }

  Bundle.prototype.set = function(key, value) {
    if (value == null) {
      value = null;
    }
    return this.values[key] = value;
  };

  Bundle.prototype.get = function(key) {
    if (key) {
      return this.values[key];
    } else {
      return this.values;
    }
  };

  return Bundle;

})();
