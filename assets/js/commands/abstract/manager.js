var Manager;

Manager = (function() {
  function Manager(options) {
    if (options == null) {
      options = {};
    }
    this.queue = options.queue || [];
    this.length = options.length || 20;
    this.pointer = -1;
  }

  Manager.prototype["do"] = function(command) {
    if (command.execute["do"]()) {
      if (this.queue.length === this.length) {
        this.queue.shift();
        this.pointer--;
      }
      this.pointer++;
      return this.queue[this.pointer] = command;
    }
  };

  Manager.prototype.undo = function() {
    if (this.pointer > -1) {
      this.queue[this.pointer].execute.undo();
      return this.pointer--;
    }
  };

  Manager.prototype.redo = function() {
    if (this.pointer + 1 < this.queue.length) {
      this.pointer++;
      return this.queue[this.pointer].execute.redo();
    }
  };

  return Manager;

})();
