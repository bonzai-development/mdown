var safeApply;

if (typeof safeApply !== 'function') {
  safeApply = function($scope, fn) {
    if ($scope.$$phase || $scope.$root.$$phase) {
      return fn();
    } else {
      return $scope.$apply(fn);
    }
  };
}

angular.module('mdown', ['ui.router']).config(function($stateProvider, $urlRouterProvider, $locationProvider) {
  console.log("angular config");
  $locationProvider.html5Mode(true);
  $urlRouterProvider.when('', '/');
  return $stateProvider.state('document:blank', {
    url: '/',
    controller: 'DocumentBlankCtrl',
    templateUrl: '/views/editor/document/blank'
  }).state('document:error', {
    url: '/error',
    controller: 'DocumentErrorCtrl',
    templateUrl: '/views/editor/document/error'
  }).state('document', {
    url: '/:key',
    controller: 'DocumentCtrl',
    templateUrl: '/views/editor/document'
  });
});
