angular.module('mdown').factory('Editor', function() {
  var Editor;
  console.log("Editor");
  Editor = (function() {
    function Editor() {
      this.callbacks = {};
    }

    Editor.prototype.load = function() {
      this.editor = CodeMirror.fromTextArea($('#editor')[0], {
        mode: "markdown",
        viewportMargin: Infinity,
        lineWrapping: true,
        autofocus: true
      });
      return this.emit('load', [this.editor]);
    };

    Editor.prototype.emit = function(event, args) {
      var callback, _i, _len, _ref, _results;
      _ref = this.callbacks[event] || [];
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        callback = _ref[_i];
        _results.push(callback.apply(this, args));
      }
      return _results;
    };

    Editor.prototype.on = function(event, callback) {
      return (this.callbacks[event] || (this.callbacks[event] = [])).push(callback);
    };

    return Editor;

  })();
  return new Editor;
});
