angular.module('mdown').factory('Document', function($http) {
  var Document;
  console.log("Document");
  Document = (function() {
    function Document() {
      this.callbacks = {};
      this.attrs = {
        editable: false
      };
    }

    Document.prototype.create = function(callback) {
      var _this = this;
      return $http.post('/api/documents', {}).success(function(response) {
        _this.attrs = response;
        _this.attrs.editable = true;
        if (typeof callback === "function") {
          callback(null, _this);
        }
        return _this.emit('create');
      }).error(function(response) {
        return typeof callback === "function" ? callback(response, null) : void 0;
      });
    };

    Document.prototype.read = function(key, callback) {
      var _this = this;
      return $http.get('/api/documents/' + key).success(function(response) {
        _this.attrs = response;
        _this.attrs.editable = true;
        if (typeof callback === "function") {
          callback(null, _this);
        }
        return _this.emit('read');
      }).error(function(response) {
        return typeof callback === "function" ? callback(response, null) : void 0;
      });
    };

    Document.prototype.update = function(callback) {
      var _this = this;
      return $http.put('/api/documents/' + this.get('key'), this.attrs).success(function(response) {
        if (typeof callback === "function") {
          callback(null, _this);
        }
        return _this.emit('update');
      }).error(function(response) {
        return typeof callback === "function" ? callback(response, null) : void 0;
      });
    };

    Document.prototype.close = function(callback) {
      this.attrs = {
        editable: false
      };
      if (typeof callback === "function") {
        callback(null, this);
      }
      return this.emit('close');
    };

    Document.prototype.editable = function() {
      return this.attrs.editable;
    };

    Document.prototype.get = function(key) {
      return this.attrs[key];
    };

    Document.prototype.set = function(key, value) {
      return this.attrs[key] = value;
    };

    Document.prototype.emit = function(event, args) {
      var callback, _i, _len, _ref, _results;
      _ref = this.callbacks[event] || [];
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        callback = _ref[_i];
        _results.push(callback.apply(this, args));
      }
      return _results;
    };

    Document.prototype.on = function(event, callback) {
      return (this.callbacks[event] || (this.callbacks[event] = [])).push(callback);
    };

    Document.prototype.toJSON = function() {
      return this.attrs;
    };

    return Document;

  })();
  return new Document;
});
