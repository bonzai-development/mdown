angular.module('mdown').factory 'Document', ($http) ->
	console.log("Document")

	class Document
		constructor: () ->
			@callbacks = {}

			@attrs = {
				editable: false
			}

		create: (callback) ->
			$http.post('/api/documents', {})
				.success (response) =>
					@attrs = response
					@attrs.editable = true

					callback?(null, @)
					@emit('create')
				.error (response) =>
					callback?(response, null)

		read: (key, callback) ->
			$http.get('/api/documents/' + key)
				.success (response) =>
					@attrs = response
					@attrs.editable = true

					callback?(null, @)
					@emit('read')
				.error (response) =>
					callback?(response, null)

		update: (callback) ->
			$http.put('/api/documents/' + @get('key'), @attrs)
				.success (response) =>
					callback?(null, @)
					@emit('update')
				.error (response) =>
					callback?(response, null)

		close: (callback) ->
			@attrs = {
				editable: false
			}
			callback?(null, @)
			@emit('close')

		editable: () ->
			@attrs.editable

		get: (key) ->
			@attrs[key]

		set: (key, value) ->
			@attrs[key] = value

		emit: (event, args) ->
			for callback in @callbacks[event] || []
				callback.apply(@, args)

		on: (event, callback) ->
			(@callbacks[event] || @callbacks[event] = []).push(callback)

		toJSON: () ->
			@attrs

	return new Document