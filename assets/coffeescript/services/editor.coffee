angular.module('mdown').factory 'Editor', () ->
	console.log("Editor")

	class Editor
		constructor: () ->
			@callbacks = {}

		load: () ->
			@editor = CodeMirror.fromTextArea($('#editor')[0], {
				mode: "markdown",
				viewportMargin: Infinity,
				lineWrapping: true,
				autofocus: true
			})

			@emit 'load', [@editor]

		emit: (event, args) ->
			for callback in @callbacks[event] || []
				callback.apply(@, args)

		on: (event, callback) ->
			(@callbacks[event] || @callbacks[event] = []).push(callback)



	return new Editor