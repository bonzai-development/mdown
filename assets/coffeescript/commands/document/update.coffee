class UpdateDocumentCommand extends Command
	init: () ->
		@required.set('document')
		@required.set('editor')
	do: () ->
		document = @params.get('document')
		editor = @params.get('editor')
		
		if document.get('content') != editor.getValue()
			@storage.set('undo', document.get('content')) # store current value
			
			document.set('content', editor.getValue()) # set new value
			document.update (err, doc) -> # save new value
				console.log("Document update do")

			return true

		return false
	undo: () ->
		document = @params.get('document')
		editor = @params.get('editor')

		@storage.set('redo', document.get('content')) # store current value
		
		document.set('content', @storage.get('undo')) # retrieve old value
		editor.setValue(document.get('content')) # set old value
		document.update (err, doc) -> # save old value
			console.log("Document update undo")
	redo: () ->
		document = @params.get('document')
		editor = @params.get('editor')

		document.set('content', @storage.get('redo')) #set new value again
		
		editor.setValue(document.get('content')) # set new value again
		document.update (err, doc) -> # save new value again
			console.log("Document update do")