class Bundle
	constructor: () ->
		@values = {}
	set: (key, value = null) ->
		@values[key] = value
	get: (key) ->
		if key
			@values[key]
		else
			@values