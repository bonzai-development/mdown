class Command
	constructor: () ->
		@params = new Bundle()
		@required = new Bundle()
		@storage = new Bundle()
		@execute = new Executor(@)
		@init?()
	do: () ->
		throw new Error("Unimplemented do method.")
	undo: () ->
		throw new Error("Unimplemented undo method.")