class Executor
	constructor: (@command) ->
		#
	isExecutable: (command) ->
		executable = true
		for key, value of command.required.get()
			executable = executable and key of command.params.get()
		executable
	getMissingParams: (command) ->
		missing = []
		for key, value of command.required.get()
			if key not of command.params.get()
				missing.push(key)
		missing
	do: () ->
		if !@isExecutable(@command)
			throw new Error("Do method is not executable. Missing params: " + @getMissingParams(@command).join(', '))
		@command.do()
	undo: () ->
		if !@isExecutable(@command)
			throw new Error("Undo method is not executable. Missing params: " + @getMissingParams(@command).join(', '))
		@command.undo()
	redo: () ->
		if !@isExecutable(@command)
			throw new Error("Redo (or do) method is not executable. Missing params: " + @getMissingParams(@command).join(', '))
		if @command.redo
			@command.redo()
		else
			@command.do()