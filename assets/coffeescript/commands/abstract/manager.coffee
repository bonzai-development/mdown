class Manager
	constructor: (options = {}) ->
		@queue = options.queue || []
		@length = options.length || 20
		@pointer = -1
	do: (command) ->
		if command.execute.do()
			if @queue.length == @length
				@queue.shift()
				@pointer--

			@pointer++
			@queue[@pointer] = command
	undo: () ->
		if @pointer > -1
			@queue[@pointer].execute.undo()
			@pointer--
	redo: () ->
		if @pointer + 1 < @queue.length
			@pointer++
			@queue[@pointer].execute.redo()
