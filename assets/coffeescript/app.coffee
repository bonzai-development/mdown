if typeof safeApply != 'function'
    safeApply = ($scope, fn) ->
        if ($scope.$$phase || $scope.$root.$$phase) then fn() else $scope.$apply(fn)

angular.module('mdown', ['ui.router']).config ($stateProvider, $urlRouterProvider, $locationProvider) ->
	console.log("angular config")

	$locationProvider.html5Mode(true)

	$urlRouterProvider.when('', '/')

	$stateProvider
		.state('document:blank', {
			url: '/',
			controller: 'DocumentBlankCtrl',
			templateUrl: '/views/editor/document/blank'
		})
		.state('document:error', {
			url: '/error',
			controller: 'DocumentErrorCtrl',
			templateUrl: '/views/editor/document/error'
		})
		.state('document', {
			url: '/:key',
			controller: 'DocumentCtrl',
			templateUrl: '/views/editor/document'
		})
