angular.module('mdown').controller 'NavigationCtrl', ($scope, $rootScope, $state, Document, Editor) ->
	console.log "NavigationCtrl"
	
	$content = $('.content-wrapper')

	$scope.document = Document.toJSON()

	Document.on 'read', () ->
		$scope.document = Document.toJSON()
	Document.on 'close', () ->
		$scope.document = Document.toJSON()

	$scope.second = {}
	$scope.third = {}

	$scope.second.open = () ->
		$scope.second.state = true
		$content.addClass('content-navigation-second')
	$scope.second.close = () ->
		$scope.second.state = false
		$scope.third.state = false
		$content.removeClass('content-navigation-second content-navigation-third')

	$scope.third.open = () ->
		$scope.second.state = true
		$scope.third.state = true
		$content.addClass('content-navigation-second content-navigation-third')
	$scope.third.close = () ->
		$scope.third.state = false
		$content.removeClass('content-navigation-second content-navigation-third')

	$scope.edit = {}
	$scope.edit.toggle = () ->
		$scope.second.open()

	$scope.edit.document = {}
	$scope.edit.document.create = () ->
		Document.create (err, doc) ->
			$state.transitionTo('document', {
				key: doc.get('key')
			})

	$scope.edit.document.update = () ->
		if Document.editable()
			Document.update () ->
				console.log "Document updated manually"

	Mousetrap.bind ['alt+space'], () ->
		if $scope.second.state
			$scope.second.close()
		else
			$scope.second.open()

	Editor.on 'load', (editor) ->
		console.log("editor load")
		editor.addKeyMap({
			"Alt-Space": () ->
				console.log("alt")
				if $scope.second.state
					$scope.second.close()
				else
					$scope.second.open()
				return false
		})

