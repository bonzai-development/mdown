angular.module('mdown')
	.controller 'DocumentCtrl', ($scope, $rootScope, $state, $http, $location, Document, Editor) ->
		console.log "DocumentCtrl"
		key = $state.params.key

		manager = new Manager


		save = _.debounce (doc) ->
			cmd = new UpdateDocumentCommand
			cmd.params.set('document', doc)
			cmd.params.set('editor', Editor.editor)

			manager.do(cmd)
		, 1000

		Editor.load()

		Mousetrap.bind ['command+z', 'ctrl+z'], () ->
			manager.undo()
			return false
		Mousetrap.bind ['command+y', 'ctrl+y'], () ->
			manager.redo()
			return false
		
		Document.close()

		Document.read key, (err, doc) ->
			if err
				$state.transitionTo('document:error')
			else
				Editor.editor.setValue(doc.get('content'))

				# editor.on 'change', _.debounce () ->
				# 	save(doc)
				# , 1000

				Mousetrap.bind ['command+s', 'ctrl+s'], () ->
					save(doc)
					return false

				Editor.editor.addKeyMap({
					"Cmd-S": () ->
						save(doc)
						return false
					"Ctrl-S": () ->
						save(doc)
						return false
					"Cmd-Z": () ->
						manager.undo()
						return false
					"Ctrl-Z": () ->
						manager.undo()
						return false
					"Cmd-Y": () ->
						manager.redo()
						return false
					"Ctrl-Y": () ->
						manager.redo()
						return false
				})

		$content = $('.content-wrapper')
		$content.on 'click', () ->
			Editor.editor.focus()