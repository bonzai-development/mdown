angular.module('mdown')
	.controller 'DocumentBlankCtrl', ($scope, Document, $state) ->
		console.log "DocumentBlankCtrl"

		Document.close()

		$scope.create = () ->
			Document.create (err, doc) ->
				$state.transitionTo('document', {
					key: doc.get('key')
				})
