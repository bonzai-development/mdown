fs = require 'fs'
jade = require 'jade'
coffeescript = require 'connect-coffee-script'
express = require 'express'
expressnamespace = require 'express-namespace'
stylus = require 'stylus'
nib = require 'nib'
mongoose = require 'mongoose'

secret = {}
try
	secret = require './../secret'
catch e

mongoose.connect(process.env.MONGOHQ_URL || secret.MONGOHQ_URL)

app = express()

app.configure () ->
	app.set 'port', process.env.PORT || 5000

	app.set 'views', __dirname + '/../views'
	app.set 'view engine', 'jade'

	app.use express.logger('dev')
	app.use express.favicon()
	app.use express.bodyParser()

	app.use '/assets/js', coffeescript({
		src: __dirname + '/../assets/coffeescript',
		dest: __dirname + '/../assets/js',
		bare: true
	})

	app.use '/assets/css', stylus.middleware({ 
		src: __dirname + '/../assets/styl',
		dest: __dirname + '/../assets/css',
		compile: (str, path, fn) ->
			stylus(str).set('filename', path).use(nib())
	})

	app.use '/assets', express.static(__dirname + '/../assets')

	app.use app.router

require('./models/document')(mongoose)
require('./api')(app, mongoose)

app.get '/views/*', (req, res, next) ->
	path = req.params[0] or 'editor'
	abspath = app.get('views') + '/' + path + '.jade'

	fs.exists abspath, (exists) ->
		if exists
			res.render(path, {
				path: path
			})
		else
			res.render('404')

app.get '/*', (req, res, next) ->
	res.render 'editor', {
		
	}

app.listen app.get('port'), () ->
	console.log "mdown listening on %d", app.get('port')