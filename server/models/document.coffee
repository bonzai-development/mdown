module.exports = (mongoose) ->
	documentSchema = new mongoose.Schema({
		key: String,
		content: String
	})

	Document = mongoose.model('Document', documentSchema)
	mongoose.models.Document = Document