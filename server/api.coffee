randomstring = require 'randomstring'

error = (res, err) ->
	res.send 503, {
		error: err
	}

module.exports = (app, mongoose) ->
	Document = mongoose.models.Document

	app.namespace '/api', () ->
		app.namespace '/documents', () ->
			app.get '/:key', (req, res, next) ->
				key = req.params.key

				Document.findOne { key: key }, (err, doc) ->
					if err
						return error(res, err)

					if !doc
						return res.send 404, {
							error: 'No document with specified key'
						}

					res.send 200, doc.toJSON()


			app.post '/', (req, res, next) ->
				key = randomstring.generate(3)

				doc = new Document({
					key: key,
					content: '## New document'
				})

				doc.save (err) ->
					if err
						return error(res, err)
					
					res.send 200, doc.toJSON()

			app.put '/:key', (req, res, next) ->
				key = req.params.key
				values = {
					content: req.body.content,
				}

				Document.findOneAndUpdate { key: key }, values, (err, doc) ->
					if err
						return error(res, err)

					if !doc
						return res.send 404, {
							error: 'No document with specified key'
						}

					res.send 200, doc.toJSON()


